let a = 1,
	b = 2,
	c = 3,
	e = 4;
let arr = { a, b, c, d, e };

let obj = {};

for (item in arr) {
	if (item) {
		obj[`${item}`] = arr[`${item}`];
	}
}

console.log(obj);
