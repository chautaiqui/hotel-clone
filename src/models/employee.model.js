const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const validator = require("validator");

const { accountStatus, accountStatusArr } = require("../customs/db/accStatus");
const { employeeType, employeeTypeArr } = require("../customs/db/employeeType");
const {
  documentStatus,
  documentStatusArr,
} = require("../customs/db/documentStatus");
const { defaultImg } = require("../customs/db/cloudinaryDefault");

const employeeSchema = new mongoose.Schema(
  {
    email: {
      type: String,
      required: true,
      trim: true,
      unique: true,
      validate(value) {
        if (!validator.isEmail(value)) {
          throw new Error("Invalid email!");
        }
      },
    },
    password: {
      type: String,
      trim: true,
      required: true,
      default: "aaaa4444",
      validate(value) {
        if (value.length < 8) {
          throw new Error("Password must be at least 8 characters");
        }

        if (!value.match(/\d/) || !value.match(/[a-zA-Z]/)) {
          throw new Error(
            "Password must contain at least one letter and one number"
          );
        }
      },
    },
    name: {
      type: String,
      trim: true,
      required: true,
    },
    img: {
      type: String,
      trim: true,
      default: defaultImg,
    },
    birthday: {
      type: Date,
      required: true,
    },
    phone: {
      type: String,
      trim: true,
      required: true,
    },
    address: {
      type: String,
      trim: true,
      required: true,
    },
    status: {
      type: String,
      trim: true,
      enum: accountStatusArr,
      default: accountStatus.AVAILABLE,
      validate(value) {
        if (!accountStatusArr.includes(value)) {
          throw new Error(`Invalid account status '${value}'.`);
        }
      },
    },
    hotel: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Hotel",
    },
    shift: {
      type: [mongoose.Schema.Types.ObjectId],
      ref: "HotelWorkingShift",
      default: [],
    },
    availableDayoffNumber: {
      type: Number,
      default: 12,
      required: true,
    },
    remainingDayoffNumber: {
      type: Number,
      default: function () {
        return this.availableDayoffNumber;
      },
      required: true,
      validate(value) {
        if (value > this.availableDayoffNumber) {
          throw new Error(
            "Remaining dayoff number must be lower than available dayoff number."
          );
        }
      },
    },
    type: {
      type: String,
      trim: true,
      enum: employeeTypeArr,
      required: true,
      validate(value) {
        if (!employeeTypeArr.includes(value)) {
          throw new Error(`Invalid employee type '${value}'.`);
        }
      },
    },
    skills: {
      type: [String],
      trim: true,
      default: [],
    },
    department: {
      type: String,
      trim: true,
      required: true,
    },
    designation: {
      type: String,
      trim: true,
      required: true,
    },
    baseSalary: {
      type: Number,
      required: true,
      default: 0,
    },
    docStatus: {
      type: String,
      trim: true,
      enum: documentStatusArr,
      default: documentStatus.AVAILABLE,
      validate(value) {
        if (!documentStatusArr.includes(value)) {
          throw new Error(`Invalid document status '${value}'.`);
        }
      },
    },
  },
  {
    timestamps: true,
  }
);

/**
 * check if input password is match with employee's password
 * @param {string} password
 * @returns {Promise<boolean>}
 */
employeeSchema.methods.isPasswordMatch = async function (password) {
  const employee = this;
  return await bcrypt.compare(password, employee.password);
  // return password == employee.password;
};

/**
 * pre save hook
 */
employeeSchema.pre("save", async function () {
  const employee = this;

  //hash password
  if (employee.$isDefault("password") || employee.isModified("password")) {
    employee.password = await bcrypt.hash(employee.password, 8);
  }
});

const Employee = mongoose.model("Employee", employeeSchema);

module.exports = Employee;
