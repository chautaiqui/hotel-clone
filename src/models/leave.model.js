const mongoose = require('mongoose');

const leaveSchema = new mongoose.Schema({
	employee: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Employee',
		required: true,
	},
	hotelShift: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'HotelWorkingShift',
		required: true,
	},
	hotel: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Hotel',
		required: true,
	},
	title: {
		type: String,
		default: 'Leave application form',
	},
	reason: {
		type: String,
	},
	date: { type: Number },
	month: { type: Number },
	year: { type: Number },
});

const Leave = mongoose.model('Leave', leaveSchema);
module.exports = Leave;
