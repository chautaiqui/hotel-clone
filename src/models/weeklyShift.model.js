const mongoose = require("mongoose");

const weeklyShift = new mongoose.Schema(
  {
    time: {
      type: Date,
    },
    hotelId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
    },
    shiftIds: {
      type: [mongoose.Schema.Types.ObjectId],
      default: [],
      required: true,
    },
  },
  { timestamps: true }
);

const WeeklyShift = mongoose.model("WeeklyShift", weeklyShift);
module.exports = WeeklyShift;
