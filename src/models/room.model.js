const mongoose = require("mongoose");

const { roomStatus, roomStatusArr } = require("../customs/db/roomStatus");
const { documentStatus, documentStatusArr } = require("../customs/db/documentStatus");

const facilitySchema = new mongoose.Schema(
    {
        facility: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Facility",
            required: true,
        },
        amount: {
            type: Number,
            default: 0,
            min: 0,
        }
    },
);

const roomSchema = new mongoose.Schema(
    {
        hotel: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Hotel"
        },
        name: {
            type: String,
            required: true,
            trim: true,
        },
        roomType: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "RoomType"
        },
        facilities: {
            type: [facilitySchema],
            required:true,
            default: [],
        },
        bookings: {
            type: [mongoose.Schema.Types.ObjectId],
            ref: "Booking",
            default: [],
        },
        status: {
            type: String,
            enum: roomStatusArr,
            default: roomStatus.AVAILABLE,
            validate(value) {
                if (!roomStatusArr.includes(value)) {
                    throw new Error(`Invalid room status '${value}'.`);
                }
            }
        },
        docStatus: {
            type: String,
            trim: true,
            enum: documentStatusArr,
            default: documentStatus.AVAILABLE,
            validate(value) {
                if (!documentStatusArr.includes(value)) {
                    throw new Error(`Invalid document status '${value}'.`);
                }
            }
        },
    },
    {
        timestamps: true,
    }
);

roomSchema.path('name').validate(async function() {
    const { hotel, name } = this;
    if (this.isModified('name')){
        const count = await mongoose.model("Room", roomSchema).countDocuments({ hotel: hotel, name: name });
        if (count > 0) throw new Error(`Room '${name}' is already exist in hotel id '${hotel}'.`);
    }
    return true;
});

const Room = mongoose.model("Room", roomSchema);

module.exports = Room;