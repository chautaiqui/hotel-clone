const mongoose = require("mongoose");
const { documentStatus, documentStatusArr } = require("../customs/db/documentStatus");

const workingShiftSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            trim: true,
            required: true,
        },
        hotel: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Hotel",
            required: true,
        },
        timeIn: {
            type: Date,
            required: true,
        },
        timeOut: {
            type: Date,
            required: true,
        },
        salaryCoefficient: {
            type: Number,
            required: true,
            validate(value) {
                if (value <= 0) {
                    throw new Error("Salary Coefficient must be greater than 0.");
                }
            }
        },
        docStatus: {
            type: String,
            trim: true,
            enum: documentStatusArr,
            default: documentStatus.AVAILABLE,
            validate(value) {
                if (!documentStatusArr.includes(value)) {
                    throw new Error(`Invalid document status '${value}'.`);
                }
            }
        },
    },
    {
        timestamps: true,
    }
);

workingShiftSchema.path('name').validate(async function() {
    const {name, hotel} = this;
    if (this.isModified('name')) {
        const count = await mongoose.model("WorkingShift", workingShiftSchema).countDocuments({hotel: hotel, name: name});
        if (count > 0) throw new Error(`Working shift ${name} is already exist in hotel id ${hotel}.`);
    }
    return true;
});

const WorkingShift = mongoose.model("WorkingShift", workingShiftSchema);
module.exports = WorkingShift;