const mongoose = require("mongoose");
const {
  documentStatus,
  documentStatusArr,
} = require("../customs/db/documentStatus");

const hotelWorkingShiftSchema = new mongoose.Schema(
  {
    hotel: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Hotel",
    },
    timeInOut: {
      type: String,
      required: true,
    },
    date: {
      type: Number,
      required: true,
    },
    month: {
      type: Number,
      required: true,
    },
    year: {
      type: Number,
      required: true,
    },
    salaryCoefficient: {
      type: Number,
      required: true,
      validate(value) {
        if (value <= 0) {
          throw new Error("Salary Coefficient must be greater than 0.");
        }
      },
    },
    employeeList: {
      type: [mongoose.Schema.Types.ObjectId],
      ref: "Employee",
      default: [],
    },
    docStatus: {
      type: String,
      trim: true,
      enum: documentStatusArr,
      default: documentStatus.AVAILABLE,
      validate(value) {
        if (!documentStatusArr.includes(value)) {
          throw new Error(`Invalid document status '${value}'.`);
        }
      },
    },
  },
  {
    timestamps: true,
  }
);

// hotelWorkingShiftSchema.path("name").validate(async function () {
//   const { name, hotel } = this;
//   if (this.isModified("name")) {
//     const count = await mongoose
//       .model("HotelWorkingShift", hotelWorkingShift)
//       .find({ hotel: hotel, name: name });
//     if (count > 0)
//       throw new Error(
//         `Working shift ${name} is already exist in hotel id ${hotel}.`
//       );
//   }
//   return true;
// });

const HotelWorkingShift = mongoose.model(
  "HotelWorkingShift",
  hotelWorkingShiftSchema
);

module.exports = HotelWorkingShift;
