const mongoose = require("mongoose");
const { voucherStatus, voucherStatusArr } = require("../customs/db/voucherStatus");
const { documentStatus, documentStatusArr } = require("../customs/db/documentStatus");

const voucherSchema = new mongoose.Schema(
    {
        hotel: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Hotel",
            required: true,
        },
        roomType: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "RoomType",
            required: true,
        },
        img: {
            type: String,
            trim: true,
            default: "",
        },
        startDate: {
            type: Date,
            default: Date.now(),
            required: true,
        },
        endDate: {
            type: Date,
            default: Date.now(),
            required: true,
        },
        discount: {
            type: Number,
            default: 0,
            required: true,
            validate(value) {
                if (value < 0) {
                    throw new Error("Voucher discount cannot be lower than 0.")
                }
            }
        },
        description: {
            type: String,
            required: true,
            trim: true,
            default: "Voucher description."
        },
        amount: {
            type: Number,
            required: true,
            default: 1,
            validate(value) {
                if (value < 1) {
                    throw new Error("Voucher amount cannot be lower than 1.");
                }
            },
        },
        remainingAmount: {
            type: Number,
            required: true,
            default: function () { return this.amount;},
            validate(value) {
                if (value < 0) {
                    throw new Error("Voucher remaining amount cannot be lower than 0.");
                }
            },
        },
        status: {
            type: String,
            trim: true,
            lowercase: true,
            enum: voucherStatusArr,
            default: voucherStatus.AVAILABLE,
            validate(value) {
                if (!voucherStatusArr.includes(value)) {
                    throw new Error(`Invalid voucher status '${value}'.`);
                }
            }
        },
        customers: {
            type: [mongoose.Schema.Types.ObjectId],
            ref: "Customer",
            default: []
        },
        usingCustomers: {
            type: [mongoose.Schema.Types.ObjectId],
            ref: "Customer",
            default: []
        },
        docStatus: {
            type: String,
            trim: true,
            enum: documentStatusArr,
            default: documentStatus.AVAILABLE,
            validate(value) {
                if (!documentStatusArr.includes(value)) {
                    throw new Error(`Invalid document status '${value}'.`);
                }
            }
        },
    },
    {
        timestamps: true,
    }
);

const Voucher = mongoose.model("Voucher", voucherSchema);


module.exports = Voucher;