const mongoose = require('mongoose');

const roomTypeReportSchema = new mongoose.Schema({
	roomType: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'RoomType',
		required: true,
	},
	bookingAmount: {
		type: Number,
		default: 0,
	},
	totalMoney: {
		type: Number,
		default: 0,
	},
});

const facilityReportSchema = new mongoose.Schema({
	facility: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Facility',
	},
	amount: {
		type: Number,
		default: 0,
	},
	action: {
		type: String,
		enum: ['add', 'delete'],
		default: 'add',
	},
	time: {
		type: Date,
		default: Date.now(),
	},
});

const employeeReportSchema = new mongoose.Schema({
	employee: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Employee',
	},
	action: {
		type: String,
		enum: ['add', 'delete'],
		default: 'add',
	},
	time: {
		type: Date,
		default: Date.now(),
	},
});

const hotelReportSchema = new mongoose.Schema(
	{
		hotel: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Hotel',
		},
		manager: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Employee',
		},
		month: {
			type: Number,
			max: 12,
		},
		year: {
			type: Number,
			min: 2020,
		},
		bookingReport: {
			amount: {
				type: Number,
				default: 0,
			},
			totalMoney: {
				type: Number,
				default: 0,
			},
		},
		roomTypeReport: {
			type: [roomTypeReportSchema],
			default: [],
		},
		facilityReport: {
			type: [facilityReportSchema],
			default: [],
		},
		employeeReport: {
			type: [employeeReportSchema],
			default: [],
		},
		ratingReport: {
			one: {
				type: Number,
				min: 0,
				default: 0,
			},
			two: {
				type: Number,
				min: 0,
				default: 0,
			},
			three: {
				type: Number,
				min: 0,
				default: 0,
			},
			four: {
				type: Number,
				min: 0,
				default: 0,
			},
			five: {
				type: Number,
				min: 0,
				default: 0,
			},
			allRating: {
				type: Number,
				min: 0,
				default: 0,
			},
			avg: {
				type: Number,
				min: 0,
				max: 5,
				default: 0,
			},
		},
		bookingRatingReport: {
			one: {
				type: Number,
				min: 0,
				default: 0,
			},
			two: {
				type: Number,
				min: 0,
				default: 0,
			},
			three: {
				type: Number,
				min: 0,
				default: 0,
			},
			four: {
				type: Number,
				min: 0,
				default: 0,
			},
			five: {
				type: Number,
				min: 0,
				default: 0,
			},
			allRating: {
				type: Number,
				min: 0,
				default: 0,
			},
			avg: {
				type: Number,
				min: 0,
				max: 5,
				default: 0,
			},
		},
	},
	{
		timestamps: true,
	}
);

const HotelReport = mongoose.model('HotelReport', hotelReportSchema);
module.exports = HotelReport;
