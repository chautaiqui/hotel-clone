const express = require('express');
const httpStatus = require('http-status');
const router = require('./admin.route');
const routes = express.Router();

routes.get('/', async (req, res) => {
	res.status(httpStatus.OK).send({ message: 'Welcome to hotelLV api!' });
});

const defaultRoutes = [
	{
		path: '/admin',
		route: require('./admin.route'),
	},
	{
		path: '/customer',
		route: require('./customer.route'),
	},
	{
		path: '/employee',
		route: require('./employee.route'),
	},
	{
		path: '/manager',
		route: require('./manager.route'),
	},
	{
		path: '/auth',
		route: require('./auth.route'),
	},
	{
		path: '/hotel',
		route: require('./hotel.route'),
	},
	{
		path: '/roomtype',
		route: require('./roomType.route'),
	},
	{
		path: '/room',
		route: require('./room.route'),
	},
	{
		path: '/booking',
		route: require('./booking.route'),
	},
	{
		path: '/booking-review',
		route: require('./bookingReview.route'),
	},
	{
		path: '/facilitytype',
		route: require('./facilityType.route'),
	},
	{
		path: '/facility',
		route: require('./facility.route'),
	},
	{
		path: '/blog',
		route: require('./blog.route'),
	},
	{
		path: '/hotel-shift',
		route: require('./hotelShift.route'),
	},
	{
		path: '/attendance',
		route: require('./attendance.route'),
	},
	{
		path: '/voucher',
		route: require('./voucher.route'),
	},
];

defaultRoutes.forEach((route) => {
	routes.use(route.path, route.route);
});

module.exports = routes;
