const express = require('express');
const validate = require('../middlewares/validate');

const { hotelShiftController } = require('../controllers');
const { hotelShiftValidation } = require('../validations');

const router = express.Router();

//View info of the shift by shift id
router
	.route('/:id')
	.get(
		validate(hotelShiftValidation.getHotelShiftById),
		hotelShiftController.getHotelShiftById
	);

// Assign employee to the shift
router.route('/:hotelshift_id/assign-emp/:emp_id').post(
	// validate(hotelShiftValidation.addEmpToHotelShift),
	hotelShiftController.addEmpToHotelShift
);

// View employees in the shift by id of shift
router
	.route('/:id/employees-in-shift')
	.get(
		validate(hotelShiftValidation.viewEmployeeInShift),
		hotelShiftController.viewEmployeeInShift
	);

module.exports = router;
