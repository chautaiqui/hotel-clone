const express = require('express');
const validate = require('../middlewares/validate');

const { blogController } = require('../controllers');
const { blogValidation } = require('../validations');

const router = express.Router();

const multer = require('multer');
const fileUpload = multer();

router.route('/').get(blogController.getAllBlog);

router
	.route('/create')
	.post(
		fileUpload.single('img'),
		validate(blogValidation.createBlog),
		blogController.createBlog
	);

router
	.route('/:id')
	.get(validate(blogValidation.getBlogById), blogController.getBlogById)
	.put(
		fileUpload.single('img'),
		validate(blogValidation.updateBlogById),
		blogController.updateBlogById
	);
// .delete(validate(blogValidation.deleteBlogById));

module.exports = router;
