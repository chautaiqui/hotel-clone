const Joi = require('joi').extend(require('@joi/date'));
const { phone, objectId } = require('./custom/custom.validation');

const createHotel = {
	body: Joi.object().keys({
		name: Joi.string().required(),
		description: Joi.string(),
		phone: Joi.string().required().custom(phone),
		// address: Joi.string().required(),
		street: Joi.string(),
		ward: Joi.string(),
		district: Joi.string(),
		province: Joi.string(),
		timeIn: Joi.date().required().format('HH:mm').utc(),
		timeOut: Joi.date().required().format('HH:mm').utc(),
		// capacity: Joi.number().integer().min(0),
		// averagePrice: Joi.number().min(0),
		imgs: Joi.any(),
	}),
};

const getHotels = {
	query: Joi.object().keys({
		// address: Joi.string(),
		// street: Joi.string(),
		// ward: Joi.string(),
		// district: Joi.string(),
		province: Joi.string(),
		capacity: Joi.number().integer().min(0),
		averagePrice: Joi.string().valid('asc', 'desc'),
		maxPrice: Joi.number(),
		minPrice: Joi.number(),
		page: Joi.number().integer().min(0),
		pageSize: Joi.number().integer().min(0),
		manager: Joi.boolean(),
	}),
};

const getHotelById = {
	params: Joi.object().keys({
		id: Joi.string().custom(objectId),
	}),
};

const updateHotelById = {
	params: Joi.object().keys({
		id: Joi.string().custom(objectId),
	}),
	body: Joi.object().keys({
		name: Joi.string(),
		description: Joi.string(),
		phone: Joi.string().custom(phone),
		street: Joi.string(),
		ward: Joi.string(),
		district: Joi.string(),
		province: Joi.string(),
		timeIn: Joi.date().format('HH:mm').utc(),
		timeOut: Joi.date().format('HH:mm').utc(),
		// capacity: Joi.number().integer().min(0),
		// averagePrice: Joi.number().min(0),
		imgs: Joi.any(),
	}),
};

const changeManager = {
	params: Joi.object().keys({
		id: Joi.string().custom(objectId),
	}),
	body: Joi.object().keys({
		manager: Joi.string().custom(objectId).required(),
	}),
};

const confirmLeave = {
	params: Joi.object().keys({
		id: Joi.string().custom(objectId),
	}),
};

const getLeaveByHotelId = {
	params: Joi.object().keys({
		id: Joi.string().custom(objectId),
	}),
};

const getLeaveBySpecificTime = {
	params: Joi.object().keys({
		id: Joi.string().custom(objectId),
	}),
	body: Joi.object().keys({
		month: Joi.number().required(),
		year: Joi.number().required(),
	}),
};

module.exports = {
	createHotel,
	getHotels,
	getHotelById,
	updateHotelById,
	changeManager,
	confirmLeave,
	getLeaveByHotelId,
	getLeaveBySpecificTime,
};
