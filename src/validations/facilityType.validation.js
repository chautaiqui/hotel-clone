const Joi = require("joi").extend(require("@joi/date"));
const { objectId } = require("./custom/custom.validation");

const createFacilityType = {
  params: Joi.object().keys({
    id: Joi.string().custom(objectId),
  }),
  body: Joi.object().keys({
    name: Joi.string().required(),
  }),
};

const getAllFacilityTypeByHotelId = {
  params: Joi.object().keys({
    id: Joi.string().custom(objectId),
  }),
};

const getFacilityTypeById = {
  params: Joi.object().keys({
    id: Joi.string().custom(objectId),
  }),
};

const updateFacilityTypeById = {
  params: Joi.object().keys({
    id: Joi.string().custom(objectId),
  }),
  body: Joi.object().keys({
    name: Joi.string(),
  }),
};

module.exports = {
  createFacilityType,
  getAllFacilityTypeByHotelId,
  getFacilityTypeById,
  updateFacilityTypeById,
};
