const Joi = require('joi').extend(require('@joi/date'));
const { objectId, password, phone } = require('./custom/custom.validation');

const createEmployee = {
	params: Joi.object().keys({
		id: Joi.string().custom(objectId),
	}),
	body: Joi.object().keys({
		email: Joi.string().required().email(),
		name: Joi.string().required(),
		birthday: Joi.date().format(['DD-MM-YYYY', 'DD/MM/YYYY']).utc(),
		phone: Joi.string().required().custom(phone),
		address: Joi.string().required(),
		skills: Joi.array().items(Joi.string()),
		department: Joi.string().required(),
		designation: Joi.string().required(),
		baseSalary: Joi.number().required(),
	}),
};

const login = {
	body: Joi.object().keys({
		email: Joi.string().required(),
		password: Joi.string().required(),
	}),
};

const getAllEmployeeByHotelId = {
	params: Joi.object().keys({
		id: Joi.string().custom(objectId),
	}),
};

const getEmployeeById = {
	params: Joi.object().keys({
		id: Joi.string().custom(objectId),
	}),
};

const updateEmployeeById = {
	params: Joi.object().keys({
		id: Joi.string().custom(objectId),
	}),
	body: Joi.object()
		.keys({
			email: Joi.string().email(),
			password: Joi.string().custom(password),
			name: Joi.string(),
			birthday: Joi.date().format(['DD-MM-YYYY', 'DD/MM/YYYY']).utc(),
			phone: Joi.string().custom(phone),
			address: Joi.string(),
			skills: Joi.alternatives().try(
				Joi.array().items(Joi.string()),
				Joi.string()
			),
			// skills: Joi.array().items(Joi.string()),
			department: Joi.string(),
			designation: Joi.string(),
			baseSalary: Joi.number(),
		})
		.min(1),
};

const getAllShiftByEmpId = {
	params: Joi.object().keys({
		id: Joi.string().custom(objectId),
	}),
};

const getAllAttendanceByEmpId = {
	params: Joi.object().keys({
		id: Joi.string().custom(objectId),
	}),
};

const getSalaryByEmpId = {
	params: Joi.object().keys({
		id: Joi.string().custom(objectId),
	}),
};

const getAttendanceByMonth = {
	params: Joi.object().keys({
		id: Joi.string().custom(objectId),
	}),
	body: Joi.object().keys({
		month: Joi.number().required(),
		year: Joi.number().required(),
	}),
};

const getShiftBySpecificTime = {
	params: Joi.object().keys({
		id: Joi.string().custom(objectId),
	}),
	body: Joi.object().keys({
		month: Joi.number().required(),
		year: Joi.number().required(),
	}),
};

const applyForLeave = {
	params: Joi.object().keys({
		shift_id: Joi.string().custom(objectId),
		emp_id: Joi.string().custom(objectId),
		hotel_id: Joi.string().custom(objectId),
	}),
	body: Joi.object().keys({
		title: Joi.string().required(),
		reason: Joi.string().required(),
		date: Joi.number().integer().required(),
		month: Joi.number().integer().required(),
		year: Joi.number().integer().required(),
	}),
};

const getLeaveByEmpId = {
	params: Joi.object().keys({
		id: Joi.string().custom(objectId),
	}),
};

module.exports = {
	createEmployee,
	login,
	getAllEmployeeByHotelId,
	getEmployeeById,
	updateEmployeeById,
	// changePassword,
	getAllShiftByEmpId,
	getAllAttendanceByEmpId,
	getSalaryByEmpId,
	getAttendanceByMonth,
	getShiftBySpecificTime,
	applyForLeave,
	getLeaveByEmpId,
};
