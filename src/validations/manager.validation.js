const Joi = require('joi').extend(require('@joi/date'));
const { objectId, password, phone } = require('./custom/custom.validation');

const createManager = {
	body: Joi.object().keys({
		email: Joi.string().required().email(),
		name: Joi.string().required(),
		birthday: Joi.date().format(['DD-MM-YYYY', 'DD/MM/YYYY']).utc(),
		phone: Joi.string().required().custom(phone),
		address: Joi.string().required(),
		skills: Joi.array().items(Joi.string()),
		department: Joi.string().required(),
		designation: Joi.string().required(),
		baseSalary: Joi.number().required(),
	}),
};

const login = {
	body: Joi.object().keys({
		email: Joi.string().required(),
		password: Joi.string().required(),
	}),
};

const getManagerById = {
	params: Joi.object().keys({
		id: Joi.string().custom(objectId),
	}),
};

const updateProfileById = {
	params: Joi.object().keys({
		id: Joi.string().custom(objectId),
	}),
	body: Joi.object()
		.keys({
			email: Joi.string().email(),
			password: Joi.string().custom(password),
			name: Joi.string(),
			birthday: Joi.date().format(['DD-MM-YYYY', 'DD/MM/YYYY']).utc(),
			phone: Joi.string().custom(phone),
			address: Joi.string(),
			skills: Joi.alternatives().try(
				Joi.array().items(Joi.string()),
				Joi.string()
			),
			department: Joi.string(),
			designation: Joi.string(),
			baseSalary: Joi.number(),
		})
		.min(1),
};

// const changePassword = {

// }

module.exports = {
	createManager,
	login,
	getManagerById,
	updateProfileById,
};
