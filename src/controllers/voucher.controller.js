const {
	voucherService,
	hotelService,
	roomTypeService,
	customerService,
	cloudinaryService,
} = require('../services');
const httpStatus = require('http-status');
const ApiEror = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');

/*const CronJob = require('cron').CronJob;

exports.simpleCronjob = catchAsync(async (req, res, next) => {
	const { time, str } = req.body;
	const date = new Date(
		time.getUTCFullYear(),
		time.getUTCMonth(),
		time.getUTCDate(),
		time.getUTCHours(),
		time.getUTCMinutes(),
		0
	);
	const job = new CronJob(
		date,
		async function (aaa) {
			console.log(aaa);
		}.bind(null, str),
		null,
		true,
		0 Africa/Abidjan
	);
	job.start();
	res.status(200).send({ time });
});*/

exports.createVoucher = catchAsync(async (req, res, next) => {
	const hotel = await hotelService.getHotelById(req.params.id);
	const roomType = await roomTypeService.getRoomTypeByNameAndHotelId(
		req.body.roomType,
		hotel._id
	);
	if (req.file) {
		req.body.img = await cloudinaryService.uploadImg(req.file);
	}
	Object.assign(req.body, {
		hotel: hotel._id,
		roomType: roomType._id,
	});
	const voucher = await voucherService.createVoucher(req.body);
	res.status(httpStatus.CREATED).send(voucher);
});

exports.getAllVoucher = catchAsync(async (req, res, next) => {
	const vouchers = await voucherService.getAllVoucher();
	res.status(httpStatus.OK).send({ vouchers });
});

exports.getAvailableVouchers = catchAsync(async (req, res, next) => {
	const vouchers = await voucherService.getAvailableVoucher();
	res.status(httpStatus.OK).send({ vouchers });
});

exports.getVouchersByHotelId = catchAsync(async (req, res, next) => {
	const vouchers = await voucherService.getVouchersByHotelId(req.params.id);
	res.status(httpStatus.OK).send({ vouchers });
});

exports.getVoucherById = catchAsync(async (req, res, next) => {
	const voucher = await voucherService.getVoucherById(req.params.id);
	res.status(httpStatus.OK).send(voucher);
});

exports.updateVoucherById = catchAsync(async (req, res, next) => {
	if (req.body.roomType) {
		const oldVoucher = await voucherService.getVoucherById(req.params.id);
		req.body.roomType = await roomTypeService.getRoomTypeByNameAndHotelId(
			req.body.roomType,
			oldVoucher.hotel._id
		);
	}
	if (req.file) {
		req.body.img = await cloudinaryService.uploadImg(req.file);
	}
	const voucher = await voucherService.updateVoucherById(
		req.params.id,
		req.body
	);
	res.status(httpStatus.OK).send(voucher);
});

exports.customerGetVoucher = catchAsync(async (req, res, next) => {
	const customer = await customerService.getCustomerById(req.body.customer);
	const voucher = await voucherService.customerGetVoucher(
		req.params.id,
		customer._id
	);
	res.status(httpStatus.OK).send(voucher);
});
