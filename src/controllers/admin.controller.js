const {
	adminService,
	tokenService,
	cloudinaryService,
} = require('../services');
const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');

module.exports.createAdmin = catchAsync(async (req, res, next) => {
	const admin = await adminService.createAdmin(req.body);
	res.status(httpStatus.CREATED).send(admin);
});

module.exports.login = catchAsync(async (req, res, next) => {
	const { email, password } = req.body;
	const admin = await adminService.loginAdminWithEmailAndPassword(
		email,
		password
	);
	const token = await tokenService.generateToken(admin._id);
	res.send({ admin, token });
});

module.exports.getAdminById = catchAsync(async (req, res, next) => {
	const admin = await adminService.getAdminById(req.params.id);
	res.send(admin);
});

module.exports.updateProfile = catchAsync(async (req, res, next) => {
	if (req.file) {
		req.body.img = await cloudinaryService.uploadImg(req.file);
	}
	const admin = await adminService.updateProfileById(req.params.id, req.body);
	res.send(admin);
});
