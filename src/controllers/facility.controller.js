const {
  facilityService,
  facilityTypeService,
  hotelService,
} = require("../services");
const httpStatus = require("http-status");
const ApiEror = require("../utils/ApiError");
const catchAsync = require("../utils/catchAsync");

module.exports.createFacility = catchAsync(async (req, res, next) => {
  const hotel = await hotelService.getHotelById(req.params.id);
  const facilityType = await facilityTypeService.getFacilityTypeByNameandHotelId(
    req.body.type,
    hotel._id
  );
  req.body.hotel = hotel._id;
  req.body.type = facilityType._id;
  const facility = await facilityService.createFacility(req.body);
  res.status(httpStatus.CREATED).send(facility);
});

module.exports.getAllFacilityByHotelId = catchAsync(async (req, res, next) => {
  const facilities = await facilityService.queryManyFacility(
    { hotel: req.params.id },
    {}
  );
  res.status(httpStatus.OK).send({ facilities });
});

module.exports.getFacilityById = catchAsync(async (req, res, next) => {
  const facility = await facilityService.getFacilityById(req.params.id);
  res.status(httpStatus.OK).send(facility);
});

module.exports.updateFacilityById = catchAsync(async (req, res, next) => {
  const facility = await facilityService.updateFacilityById(
    req.params.id,
    req.body
  );
  res.status(httpStatus.OK).send(facility);
});
