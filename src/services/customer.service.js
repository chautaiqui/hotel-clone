const { Customer, Voucher, Booking } = require('../models');
const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');

const createCustomer = async (customerBody) => {
	const customer = await Customer.create(customerBody);
	return customer;
};

const loginCustomerWithEmailAndPassword = async (email, password) => {
	const customer = await Customer.findOne({ email: email }).populate(
		'voucher'
	);
	if (!customer || !(await customer.isPasswordMatch(password))) {
		throw new ApiError(
			httpStatus.UNAUTHORIZED,
			'Incorrect email or password.'
		);
	}
	return customer;
};

const getAll = async () => {
	const customers = await Customer.find({});
	return customers;
};

const getCustomerById = async (customerId) => {
	const customer = await Customer.findById(customerId).populate('voucher');
	if (!customer) {
		throw new ApiError(
			httpStatus.NOT_FOUND,
			`Not found customer with id ${customerId}.`
		);
	}
	return customer;
};

const getVouchersByCustomerId = async (customerId) => {
	const customer = await Customer.findById(customerId);
	if (!customer) {
		throw new ApiError(
			httpStatus.NOT_FOUND,
			`Not found customer with id ${customerId}.`
		);
	}
	const vouchers = await Voucher.find({ _id: { $in: customer.voucher } });
	return vouchers;
};

const getBookingsByCustomerId = async (customerId) => {
	const customer = await Customer.findById(customerId);
	if (!customer) {
		throw new ApiError(
			httpStatus.NOT_FOUND,
			`Not found customer with id ${customerId}.`
		);
	}

	const bookings = await Booking.find({ customer: customer._id })
		.populate('voucher')
		.populate({ path: 'room', populate: { path: 'roomType' } });
	return bookings;
};

const updateProfileById = async (customerId, updateBody) => {
	const customer = await getCustomerById(customerId);
	Object.assign(customer, updateBody);
	await customer.save();
	return customer;
};

// const changePassword = async (customerId, newPassword) => {
//     const customer = await updateProfileById(customerId, { password: newPassword });
//     return customer;
// };

module.exports = {
	createCustomer,
	loginCustomerWithEmailAndPassword,
	getAll,
	getCustomerById,
	updateProfileById,
	getVouchersByCustomerId,
	getBookingsByCustomerId,
};
