const { RoomType } = require("../models");
const httpStatus = require("http-status");
const ApiError = require("../utils/ApiError");

const createRoomType = async (roomTypeBody) => {
    const roomType = await RoomType.create(roomTypeBody);
    return roomType;
};

const getAllRoomTypeByHotelId = async (hotelId) => {
    const roomTypes = await RoomType.find({ hotel: hotelId });
    return roomTypes;
};

const getRoomTypeById = async (roomTypeId) => {
    const roomType = await RoomType.findById(roomTypeId);
    if (!roomType) {
        throw new ApiError(httpStatus.NOT_FOUND, `Not found room type with id '${roomTypeId}'`);
    }
    return roomType;
};

const getRoomTypeByNameAndHotelId = async (name, hotelId) => {
    const roomType = await RoomType.findOne({name: name, hotel: hotelId});
    if (!roomType) {
        throw new ApiError(httpStatus.NOT_FOUND, `Not found room type '${name}' of hotel id '${hotelId}'`);
    }
    return roomType;
};

const updateRoomTypeById = async (roomTypeId, updateBody) => {
    const roomType = await getRoomTypeById(roomTypeId);
    Object.assign(roomType, updateBody);
    await roomType.save();
    return roomType;
};

module.exports = {
    createRoomType,
    getAllRoomTypeByHotelId,
    getRoomTypeById,
    getRoomTypeByNameAndHotelId,
    updateRoomTypeById,
}