const multer = require("multer");

// define storage for the img
const storage = multer.diskStorage({
    //destination for files
    destination: (req, file, callback) => {
        callback(null, "./public");
    },
    //add back the extension
    filename: (req, file, callback) => {
        callback(null, Date.now() + file.originalname);
    },
});
  
//upload parameters for multer
exports.upload = multer({
    storage: storage,
    limits: {
        fieldSize: 1024 * 1024 * 4,
    },
});